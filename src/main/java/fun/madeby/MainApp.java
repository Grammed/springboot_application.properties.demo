package fun.madeby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class MainApp {

	public static void main(String[] args) {

	//SpringApplication.run(MainApp.class, args);
		ConfigurableApplicationContext ctx = SpringApplication.run(MainApp.class, args);

		System.out.println(ctx.getEnvironment().getProperty("disney.character"));
		System.out.println(ctx.getBean(Environment.class).getProperty("disney.character"));
}
}


